var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var neo4j = require('neo4j-driver');

var app = express();

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

const config = {
  "neo4j": {
    "url": "bolt://localhost:7687",
    "authUser": "neo4j",
    "authKey": "graph"
  }
}

const driver = neo4j.driver(
  config.neo4j.url,
  neo4j.auth.basic(config.neo4j.authUser, config.neo4j.authKey)
);

const session = driver.session();

//CREATE A NEW COLLECTION
app.post('/create/collection', function (req, res) {
  let owner = req.body.owner;
  let name = req.body.name;
  let description = req.body.description;
  let id = req.body.id;
  let collaborators = req.body.collaborators;

  session
    .run('CREATE (n:Collection {owner: $ownerParam, name: $nameParam, description: $descriptionParam, id: $idParam, collaborators: $collaboratorsParam}) Return n',
      ({ ownerParam: owner, nameParam: name, descriptionParam: description, idParam: id, collaboratorsParam: collaborators }))
    .then(result => {
      res.redirect('/');
    })
    .catch(e => console.log('Error Creating Nodes', e))
})

//CREATE PARENT RELATION BETWEEN TWO NODES
app.post('/create/parent/relation', function (req, res) {
  let parentID = req.body.parentID;
  let childID = req.body.childID;

  session
    .run('MATCH (a:Collection), (b:Collection) WHERE a.id = $aID AND b.id = $bID MERGE (a)-[r:PARENT_OF]->(b) RETURN a,r,b', { aID: parentID, bID: childID })
    .then(result => {
      res.redirect('/');
    })
    .catch(e => console.log('Error Creating Relations', e))
})

//CREATE CHILD RELATION BETWEEN TWO NODES
app.post('/create/child/relation', function (req, res) {
  let childID = req.body.childID;
  let parentID = req.body.parentID;

  session
    .run('MATCH (a:Collection), (b:Collection) WHERE a.id = $aID AND b.id = $bID MERGE (a)-[r:CHILD_OF]->(b) RETURN a,r,b', { aID: childID, bID: parentID })
    .then(result => {
      res.redirect('/');
    })
    .catch(e => console.log('Error Creating Relations', e))
})

//COPY OPERATION
app.post('/copy', function (req, res) {
  let fromNode = req.body.fromNode;
  let toNode = req.body.toNode;

  session
    .run(`MATCH (from:Collection {name: "Historic"}), (to: Collection {name: "Essential"})
          CALL apoc.path.subgraphAll(from, {
            relationshipFilter: "PARENT_OF>,<CHILD_OF,CHILD_OF>",
            filterStartNode: false
          })
          YIELD nodes, relationships
          CALL apoc.refactor.cloneSubgraph(
            nodes, relationships
          )
          YIELD input, output, error
          MATCH (copy {name: from.name}) WHERE NOT EXISTS((copy)-[:CHILD_OF]->())
          SET	copy.name = "Copy of " + from.name
          MERGE (to)-[:PARENT_OF]->(copy)
          MERGE (copy)-[:CHILD_OF]->(to)
          WITH to, input, output, error, copy
          MATCH p = (copyNode {name: copy.name})-[*]-()
          FOREACH (n IN nodes(p) | SET n.collaborators = to.collaborators)
          FOREACH (n IN nodes(p) | SET n.id = apoc.create.uuid() + 1)
          RETURN input, output, error;`
      , { fromId: fromNode, toId: toNode })
    .then(result => {
      //console.log(result)
      res.send('ok')
    })
    .catch(e => console.log('Error in Copying: ', e));
})

//MOVE OPERATION
app.post('/move', function (req, res) {
  let source = req.body.source;
  let destination = req.body.destination;

  session
    .run(`MATCH (source {id: $sourceId}), (destination {id: $destId}), (source)-[:CHILD_OF]->(prevParent)
          MATCH (source)-[r:CHILD_OF]->(prevParent)-[f:PARENT_OF]->(source)
          DELETE r, f
          CREATE (source)-[:CHILD_OF]->(destination)
          CREATE (source)<-[:PARENT_OF]-(destination)
          WITH source, destination
          OPTIONAL MATCH path = (source)-[:PARENT_OF*]-()
          FOREACH (node IN nodes(path) | SET node.collaborators = destination.collaborators)`
      , { sourceId: source, destId: destination })
    .then(result => {
      res.send('ok')
    })
    .catch(e => console.log('Error in Moving: ', e));
})

//DEFAULT: GET ALL NODES
app.get('/', function (req, res) {
  session
    .run('MATCH (n) RETURN n')
    .then((result) => {
      let collectionArr = [];
      result.records.forEach(record => {
        collectionArr.push(record._fields[0])
      });
      res.render('index', {
        collections: collectionArr
      });
    })
    .catch(error => {
      console.log('ERROR BODY: ', error)
    })
})

//GET ALL DECENDENTS OF A NODE
app.get('/tree/collection/:name', function (req, res) {
  let collection = req.params.name;
  let data = [];

  session
    .run(`MATCH (p:Collection {name: $collParam})
          CALL apoc.path.expandConfig(p, {
            relationshipFilter: "PARENT_OF>"
          })
          YIELD path
          RETURN [node in nodes(path) | node.name] AS nodes, length(path) AS hops
          ORDER BY hops;`
      , { collParam: collection })
    .then(result => {
      result.records.forEach(record => {
        data.push(record._fields[0])
      })
      let tree = createTree(data)
      res.send(tree)
    })
    .catch(e => console.log('Error Getting Collection Tree: ', e))
})

//data: [[parentName, childName]...]
createTree = (data) => {
  let result = data.reduce((r, keys) => {
    keys.reduce((obj, key) => obj[key] = obj[key] || {}, r);
    return r;
  }, {});
  return result;
}

/* WORK IN PROGRESS */
//GET NODES AND THEIR RELATIONS WHERE A SPECIFIC COLLABORATOR IS FOUND
app.get('/tree/collaborator/:name', function (req, res) {
  let collaborator = req.params.name;
  let infoArray = [];

  // MATCH (f) WHERE f.owner=$collParam
  // WITH f
  // MATCH (n) WHERE $collParam IN n.collaborators
  // WITH f,n
  // OPTIONAL MATCH (n)-[r:PARENT_OF]->(m)
  // WITH f,n,r,m
  // OPTIONAL MATCH (f)-[r1:PARENT_OF]->(m1)
  // RETURN n,r,m,f,r1,m1

  session
    .run(`
          MATCH (n) WHERE $collParam IN n.collaborators
          WITH n
          OPTIONAL MATCH (n)-[r:PARENT_OF*]->(m)
          RETURN n,r,m`
      , { collParam: collaborator })
    .then(result => {
      result.records.forEach(record => {
        let innerArray = []
        innerArray.push(record._fields[0].properties.name, record._fields[1].length, record._fields[2].properties.name)
        infoArray.push(innerArray)
      })
      let tree = createTree(infoArray, collection)
      res.send(tree);
    })
    .catch(e => console.log('Error Getting Collaborator Tree: ', e))
})

//DELETE COLLECTION
app.delete('/delete/collection/:id', function (req, res) {
  let collection = req.params.id;

  session
    .run(`MATCH (node:Collection {id: $collID})-[:PARENT_OF*]->(x)
          WITH DISTINCT x,node
          DETACH DELETE x, node`
      , { collID: collection })
    .then(result => {
      res.send('ok')
    })
    .catch(e => console.log('Error while deleting: ', e))
})

//REMOVE COLLABORATOR FROM COLLECTION AND SUB-COLLECTIONS
app.post('/remove/collaborator', function (req, res) {
  let collection = req.body.collection;
  let collaborator = req.body.collaborator;

  session
    .run(`MATCH path = (node:Collection {name: $collectionName})-[:PARENT_OF*]->(x)
          WITH DISTINCT x, node, path
          FOREACH (n in nodes(path) | SET n.collaborators = [x IN n.collaborators WHERE x <> $collaboratorName])`
      , { collectionName: collection, collaboratorName: collaborator })
    .then(result => {
      res.send('ok')
    })
    .catch(e => console.log('Error while removing: ', e))
})

app.listen(3000);
console.log('Server Started on Port 3000');

module.exports = app;
